package com.serverless;

import java.util.Collections;
import java.util.Optional;
import java.util.Map;

import org.apache.log4j.Logger;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Handler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {
	private static final Logger LOG = Logger.getLogger(Handler.class);

	@Override
	public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
		LOG.info("received: " + input);
		@SuppressWarnings("unchecked")
		Optional<Map<String, String>> query = Optional.ofNullable((Map<String, String>) input.get("queryStringParameters"));
		String name = query.orElse(Collections.emptyMap()).getOrDefault("name", "World");
		String hello = "Hello " + name + "!";
		Response responseBody = new Response(hello, input);
		// return ApiGatewayResponse.builder().setStatusCode(200).setObjectBody(responseBody).build();
		return ApiGatewayResponse.builder().setStatusCode(200).setObjectBody(hello).build();
	}
}
