# JCon 2018 Examples

## jcon-java

Simple Spring Boot Application for demo. Simple Controller greeting the world.

## jcon-ansible

Simple Vagrant Box which installs jcon-java app with ansible.

## jcon-docker

Simple Dockerfile installing the jacon-java app in a docker container.

## jcon-k8s

Kubernetes Deployment file for Docker container

## jcon-java-serverless

Simple request handler which can be deployed as AWS Lambda. 
