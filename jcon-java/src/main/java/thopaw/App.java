
package thopaw;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Value;

@SpringBootApplication
@Controller
public class App {

  public static void main(String... args) {
    SpringApplication.run(App.class, args);
  }

  @Value("#{systemProperties['PID']}")
  String pid;

  @GetMapping(value = { "/", "/hello" })
  @ResponseBody
  public String getHello(@RequestParam(name = "name",
      required = false, defaultValue = "World") String name) {
    return "[ PID:" + pid + "] " + "Hello " + name + "!";
  }
}
